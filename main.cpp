#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include "LIEF/LIEF.hpp"
#include "LIEF/logging.hpp"
#include "LIEF/PE/signature/SignatureParser.hpp"
#include "LIEF/PE/signature/Signature.hpp"

using namespace LIEF;
using namespace PE;

int main(int agvc, char** argv) {
    char* filename = argv[1];
//    logging::enable();
//    logging::set_level(LIEF::logging::LOG_DEBUG);

    std::unique_ptr<PE::Binary> app = PE::Parser::parse(std::string(filename));
    std::cout << app << std::endl;

    std::cout << "Has signatures: " <<  app->has_signatures() << std::endl;
    /*
    auto pe_hash = app->authentihash(PE::ALGORITHMS::SHA_256);
    std::cout << "Hash: ";
    std::for_each(pe_hash.cbegin(), pe_hash.cend(), [](const auto val) { std::cout << unsigned(val); });
    std::cout << std::endl;
    */

    //std::ifstream file(R"(C:\Users\aleksandr.yusuk\Desktop\certs\authenticode.spc)");
    //std::vector<uint8_t> cert((std::istream_iterator<uint8_t>(file)), std::istream_iterator<uint8_t>());

    // auto signatures = SignatureParser::parse(cert, false);

    auto signatures = SignatureParser::parse(R"(C:\Users\aleksandr.yusuk\Desktop\certs\authenticode.spc)");

    app->add_singature(signatures.value());

    std::cout << "Has signatures: " <<  app->has_signatures() << std::endl;
    std::cout << "\n\n\n Signature\n" << signatures.value() << std::endl;
    Builder builder = Builder(app.get());
    builder.build();
    builder.write(R"(C:\Users\aleksandr.yusuk\Desktop\SignedWaykCseClone.exe)");
}
